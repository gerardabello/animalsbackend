package db


type User struct{
    Id int              `db:"id"`
    Name string         `db:"nombre"`
    Id_vet int          `db:"idvet"`
    Direccion string
    Ciudad string
    CPostal int
    Pais string
    Region string       `db:"provincia"`
    Telf string
    Permisos int        `db:"permisos"`
}

type Mascota struct{
    Id int              `db:"id"`
    Nombre string       `db:"nombre"`
    Id_usuario int      `db:"idusuario"`
    Id_raza int         `db:"idraza"`
    Peso float32        `db:"peso"`
    Nacimiento int64    `db:"nacimiento"`
    Sexo string         `db:"sexo"`
    Foto string         `db:"foto"`
    Descripcion string  `db:"descripcion"`
    Chip string         `db:"nchip"`
}

type Veterinario struct{
    Id int
    Subdominio string
    Inicio string
    Limite string
    Nombre string
    Direccion string
    Cpostal string
    Ciudad string
    Provincia string
    Pais string
    Telf string
    Logo string
}

type Diagnostico struct{
    Id int              `db:"id"`
    IdFicha int         `db:"idficha"`
    Descripcion string  `db:"descr"`
    Tratamiento string  `db:"tratamiento"`
}

type Ficha struct{
    Id int              `db:"id"`
    IdMascota int       `db:"idmascota"`
    Fecha int64         `db:"fecha"`
}

type Foto struct{
    Id int              `db:"id"`
    Id_mascota int      `db:"idmascota"`
    Path string         `db:"path"`
}

