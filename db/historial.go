package db



const (
    baseQueryGetFicha = "SELECT id, idmascota, fecha FROM hist_fichas "
    baseQueryGetDiag = "SELECT id, idficha, descr, tratamiento FROM hist_diag "

    baseQueryPostFicha = "INSERT INTO hist_fichas (id, idmascota, fecha) VALUES (:id, :idmascota, :fecha)"

    baseQueryUpdFicha = "UPDATE hist_fichas "
)

func GetFichas(id int) ([]*Ficha){
    fichas := []*Ficha{}

    getModels(&fichas, baseQueryGetFicha + "WHERE idmascota = ?", id)

    return fichas
}


func GetDiags(id int) ([]*Diagnostico){
    diags := []*Diagnostico{}

    getModels(&diags, baseQueryGetDiag+ "WHERE idficha = ?", id)

    return diags
}

