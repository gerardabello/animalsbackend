package main

import(
    "net/http"
    /*
    "encoding/json"
    "fmt"
    */

    "animalsbackend/db"
    "github.com/gorilla/mux"
    "io/ioutil"
    "animalsbackend/log"
    "encoding/json"
    "reflect"
    "fmt"
    "strconv"
)

type Securer_post struct {
    //handler    Handler_post
    t reflect.Type
}

func (s *Securer_post) ServeHTTP(w http.ResponseWriter, r *http.Request) {

    //llegim contingut
    body, err := ioutil.ReadAll(r.Body)

    if(err!=nil){
        log.Loge(err)
    }

    //Agafem sessio
    session := db.GetSession(r)

    //mirem que tingui asignat userid
    userid, ta_ok := session.Values["userid"].(int)
    if(!ta_ok){
        http.Error(w, "Bad cookie or not logged in", 401)
        return
    }

    //agafem objecte usuari
    user := db.GetUser( userid )

    //creem estructura per guardar dades json temporalment
    var input []json.RawMessage


    //fem unmarshal del body
    err = json.Unmarshal(body,&input)

    if(err!=nil){
        log.Loge(err)
    }


    //mirem el tipus
    var innerType = s.t

    var in map[string]interface{}
    var ob interface{}

    for _, r := range input {

        err = json.Unmarshal(r,&in)

        if(err!=nil){
            log.Loge(err)
        }

        //intentem agafar la id
        idi , ok := in["Id"]

        id, ok2 := strconv.Atoi(fmt.Sprint(idi))

        //creem memoria
        ob = reflect.New(innerType).Interface()

        //Si no hi ha Id o és zero, significa que es vol crear una entrada nova. En cas contrari, es vol actualitzar l'entrada amb aquella Id
        if(!ok || ok2!=nil || id == 0){
            //NOU


            //S'agafen les dades del handler

            //Es fa un unrmarshall, aquest cop cap a un SR
            //TODO potser és més ràpid passar de map[string]interface{} a SR i no cal tornar a fer unmarshal?
            err = json.Unmarshal(r,ob)

            if(err!=nil){
                log.Loge(err)
            }

            obsr := ob.(db.SecuredResource)

            if(obsr.CanPut(user)){
                db.PostGeneric(ob)
            }else{
                http.Error(w, "Alguno de los objectos enviados no se ha registrado ya que no tienes suficientes permisos o no estas logeado", 401)
            }

        }else{
            //UPDATE


            err = db.GetGeneric(ob,id)

            if(err==nil){


                obsr := ob.(db.SecuredResource)

                if(obsr.CanUpd(user, in)){
                    db.UpdateGeneric(&in,innerType)
                }else{
                    http.Error(w, "Alguno de los objectos enviados no se ha registrado ya que no tienes suficientes permisos o no estas logeado", 401)
                }
            }else{
                http.Error(w, "Error actualizando objecto: " + err.Error(), 401)
            }
        }
    }
}


type Handler_post func() (interface{})

func AddPostRes(r *mux.Router, route string, t reflect.Type){
    r.Handle(route, &Securer_post{t}).Methods("POST")
}



