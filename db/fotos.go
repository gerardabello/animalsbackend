package db

import(
    "strconv"
)


const(
    baseQueryGetFoto = "SELECT id, idmascota, path FROM fotos "


    baseQueryPostFoto = "INSERT INTO fotos (idmascota, path) VALUES (:idmascota, :path)"
    baseQueryDeleteFoto = "DELETE FROM fotos "
)


func GetFotosMascota(id int) ([]*Foto){
    fotos := []*Foto{}

    getModels(&fotos, baseQueryGetFoto + "WHERE idmascota=?", id)

    return fotos
}


func GetFoto(id int) ([]*Foto){
    foto := []*Foto{}

    getModels(&foto, baseQueryGetFoto + "WHERE id=?", id)

    return foto
}


func PostFotos(f []*Foto){

    foto := &Foto{}
    for i := 0; i < len(f); i++ {
        foto = f[i]

        postModels(foto, baseQueryPostFoto)

    }
}

func DeleteFoto(f *Foto){
    exec(baseQueryDeleteFoto + "WHERE id="+strconv.Itoa(f.Id))
}

