package main

import(
    "net/http"
    "animalsbackend/db"
    "github.com/gorilla/mux"
    "strconv"
)

func hDeleteFoto(w http.ResponseWriter, r *http.Request){
    user := db.GetSessionUser(r)

    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "foto id must be a number", 400)
    }

    f := db.GetFoto(id)[0]

    if(f.CanDel(user)) {
        db.DeleteFoto(f)
    }
}
