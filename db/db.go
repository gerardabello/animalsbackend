package db

import (
    "animalsbackend/conf"
    _ "github.com/go-sql-driver/mysql"
    "github.com/jmoiron/sqlx"
    "animalsbackend/log"
    "reflect"
    "errors"
)

func getDB() (*sqlx.DB, error){
    return sqlx.Open("mysql", conf.MySqlConn)
}


//Basics

func getModels(dest interface{}, query string, args ...interface{}) error {

    db, err := getDB()
    if err != nil {
        log.Loge(err)
    }
    defer db.Close()

    err = db.Select(dest, query, args...)
    if err != nil {

        log.Log("getModels: error db.Select:")
        log.Loge(err)
    }


    return err
}

func getModel(dest interface{}, query string, args ...interface{}) error {

    db, err := getDB()
    if err != nil {
        log.Loge(err)
    }
    defer db.Close()

    err = db.Get(dest, query, args...)
    if err != nil {

        log.Log("getModel: error db.Select:")
        log.Loge(err)
    }

    return err
}

func postModels(in interface{},query string) {

    db, err := getDB()
    if err != nil {
        log.Loge(err)
    }
    defer db.Close()

    _, err = db.NamedExec(query, in)

    if err != nil {
        log.Loge(err)
    }
}


func exec(query string){

    db, err := getDB()
    if err != nil {
        log.Loge(err)
    }
    defer db.Close()

    db.Execf(query)
}



//####GENERICS

func GetGenerics(i interface{}, id int){
    s := reflect.Indirect(reflect.ValueOf(i))

    if(s.Kind() != reflect.Slice){
        log.Log("GetGenerics: No es un slice")
        return
    }

    //String del tipus
    bq, err := getBaseQuery(s.Type().Elem().Elem())

    if(err!=nil){
        log.Log("GetGenerics: " + err.Error())
    }

    getModels(i, bq + "WHERE id=?", id)
}


func GetGeneric(i interface{}, id int) error {
    s := reflect.Indirect(reflect.ValueOf(i))

    if(s.Kind() != reflect.Struct){
        log.Log("GetGeneric: No es un struct")
        return errors.New("GetGeneric: No es un struct")
    }

    //String del tipus
    bq, err := getBaseQuery(s.Type())
    if(err!=nil){
        log.Log("GetGeneric: " + err.Error())
    }

    err = getModel(i, bq + "WHERE id=?", id)

    return err
}



func PostGenerics(i interface{}){
    s := reflect.Indirect(reflect.ValueOf(i))

    if(s.Kind() != reflect.Slice){
        log.Log("PostGenerics: No es un slice")
        return
    }


    //String del tipus
    bq, err := postBaseQuery(s.Type().Elem().Elem())

    if(err!=nil){
        log.Log("PostGenerics: " + err.Error())
    }

    postModels(i, bq)
}

func PostGeneric(i interface{}) {
    s := reflect.Indirect(reflect.ValueOf(i))

    //String del tipus

   if(s.Kind() != reflect.Struct){
        log.Log("PostGeneric: No es un struct")
        return
    }

    //String del tipus
    bq, err := postBaseQuery(s.Type())
    if(err!=nil){
        log.Log("GetGeneric: " + err.Error())
    }


    postModels(i, bq)
}

func UpdateGeneric(in *map[string]interface{}, t reflect.Type){

    var bq string
    switch t {
    case reflect.TypeOf((*Ficha)(nil)).Elem():
        bq = baseQueryUpdFicha
        /* case reflect.TypeOf((*Foto)(nil)).Elem():
        bq = baseQueryPostFoto
    case reflect.TypeOf((*Diagnostico)(nil)).Elem():
        bq = baseQueryPostDiag
    case reflect.TypeOf((*Mascota)(nil)).Elem():
        bq = baseQueryPostMascota
    case reflect.TypeOf((*User)(nil)).Elem():
        bq = baseQueryPostUser
    case reflect.TypeOf((*Veterinario)(nil)).Elem():
        bq = baseQueryPostVet
        */
    default:
        log.Log("UpdateGenerics: No s'ha trobat cap tipus compatible : " + t.String())
        return
    }

    exec(bq + UpdateQuery(in, t))
}

