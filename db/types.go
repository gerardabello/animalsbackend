package db

import "reflect"

var TFicha reflect.Type = reflect.TypeOf((*Ficha)(nil)).Elem()
var TFoto reflect.Type =  reflect.TypeOf((*Foto)(nil)).Elem()
var TDiag reflect.Type =  reflect.TypeOf((*Diagnostico)(nil)).Elem()
var TMascota reflect.Type =  reflect.TypeOf((*Mascota)(nil)).Elem()
var TUser reflect.Type =  reflect.TypeOf((*User)(nil)).Elem()
var TVet reflect.Type =  reflect.TypeOf((*Veterinario)(nil)).Elem()

