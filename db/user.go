package db

import (
    "animalsbackend/log"
    "errors"
)


const(
    baseQueryGetUser = "SELECT id, idvet, nombre, permisos, direccion, ciudad, cpostal, provincia, pais, telf FROM usuarios "
)


//##### QUERIES

func AddUser(u *User, pass string){

    db, err := getDB()
    if err != nil {
        log.Loge(err)
        return
    }
    defer db.Close()

    tx, err := db.Begin()
    if err != nil {
        log.Loge(err)
    }
    stmt, err := tx.Prepare("insert into users(id, nombre, idvet) values(?, ?, ?)")
    if err != nil {
        log.Loge(err)
    }
    defer stmt.Close()

    _, err = stmt.Exec(u.Id, u.Name, u.Id_vet)
    if err != nil {
        log.Loge(err)
    }
    tx.Commit()
}


func GetUser(user_id int) (*User){
    user := User{}

    getModel(&user, baseQueryGetUser + "WHERE id=?", user_id)

    return &user
}

func FindUserPass(name string, pass string) (int, error){
       db, err := getDB()
    if err != nil {
        log.Loge(err)
    }
    defer db.Close()


    rows, err := db.Query("select idusuario,user,pass from login where user = '" + name + "' and pass = '" + pass + "'")
    if err != nil {
        log.Loge(err)
        return 0, err
    }
    defer rows.Close()
    rows.Next()

    var id int
    var username string
    var password string
    rows.Scan(&id,&username,&password)

    if(id == 0 ){ //El putu sql torna un usuari buit encara que no trobi res
        return 0, errors.New("User not found or password incorrect")
    }

    return id, nil
}


func GetAllUsers() ([]*User){
    users := []*User{}

    getModels(&users, baseQueryGetUser)

    return users
}
