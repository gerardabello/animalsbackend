package main

import (
    "animalsbackend/db"
    "animalsbackend/log"
    "net/http"
    "strconv"
    "github.com/gorilla/mux"
    "fmt"
)

//##### TYPES

type user_credentials struct{
    Username string
    Password string
}



//##### UTILS

func setCommonHeaders(w http.ResponseWriter){
    //w.Header().Set("Access-Control-Allow-Origin", "http://www.webveterinario.com")
    //w.Header().Set("Access-Control-Allow-Credentials", "true")

    w.Header().Set("Content-Type", "charset=utf-8")
}


//##### HANDLERS

func handlerLogin(w http.ResponseWriter, r *http.Request){
    setCommonHeaders(w)

    username := r.FormValue("username")
    password := r.FormValue("password")
    recordar := r.FormValue("recordar")



    //Inicia Sessió
    session := db.GetSession(r)


    //Busca si l'usuari i contrasenya son correctes
    id, err := db.FindUserPass(username, password)

    if err!=nil{
        //Si no son correctes no autentica
        http.Error(w, err.Error(), 403)
        log.Log(err.Error())
    }else{
        //Si son correctes autentica i guarda la id del usuari
        user := db.GetUser(id)
        session.Values["auth"] = true
        session.Values["userid"] = id
        session.Values["vetid"] = user.Id_vet
        if(recordar == "true"){
            session.Options.MaxAge = 0   //borrar al tancar el navegador
        }else{
            session.Options.MaxAge = 86400 * 7 * 3    //3 setmanes en segons 
        }
        session.Save(r, w)

        log.Log("Logged in: " + username)

    }

}

func handlerLogout(w http.ResponseWriter, r *http.Request){
    setCommonHeaders(w)

    //Inicia Sessió
    session := db.GetSession(r)

    //Fer log out
    session.Values["auth"] = false;
    session.Values["userid"] = 0

    session.Options.MaxAge = -1  //un valor negatiu indica que s'ha de borrar ja

    // Guardar canvis de la sessió.
    session.Save(r, w)
}


func handlerUsuarioId(w http.ResponseWriter, r *http.Request){
    setCommonHeaders(w)

    //Inicia Sessió
    session := db.GetSession(r)

    if(session.Values["auth"] == true){
        fmt.Fprint(w,strconv.Itoa(session.Values["userid"].(int)))
    }else{
        http.Error(w, "Not logged in", 403)
    }

}


//###RESOURCES

func handlerAllUsers(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)

    return db.GetAllUsers()
}


func handlerFotosMascota(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)

    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "pet id must be a number", 400)
        return nil
    }

    return db.GetFotosMascota(id)
}


func handlerFoto(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)

    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "foto id must be a number", 400)
        return nil
    }

    return db.GetFoto(id)
}



func handlerMascota(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)

    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "pet id must be a number", 400)
        return nil
    }


    return db.GetMascota(id)
}

func handlerUserId(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)

    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "user id must be a number", 400)
        return nil
    }

    ret := []db.User{}
    ret = append(ret,*db.GetUser( id ))
    return ret
}

func handlerMascotaDeUsuario(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)


    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "user id must be a number", 400)
        return nil
    }

    return db.GetMascotasDeUsuario(int(id))

    /*
    var interfaceSlice []db.SecuredResource = make([]db.SecuredResource, len(*data))
    for i, d := range *data {
        interfaceSlice[i] = &d
    }
    */

}

func handlerVeterinario(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)

    vars := mux.Vars(r)
    ids := vars["id"]

    id, err := strconv.Atoi(ids)
    if(err!=nil){
        return db.GetVeterinarioSub(ids)
    }else{
        return db.GetVeterinario(id)
    }

}


func handlerFichas(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)


    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "user id must be a number", 400)
        return nil
    }

    fichas := []*db.Ficha{}

    //return db.GetFichas(id)
    db.GetGenerics(&fichas,id)
    return fichas
}

func handlerDiags(w http.ResponseWriter, r *http.Request) interface{} {
    setCommonHeaders(w)


    vars := mux.Vars(r)
    id, err := strconv.Atoi(vars["id"])
    if(err!=nil){
        http.Error(w, "user id must be a number", 400)
        return nil
    }

    return db.GetDiags(id)
}

