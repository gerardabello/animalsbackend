package main

import(
    "animalsbackend/log"
    "reflect"
    "errors"
)


//Actualiza el valors valors que hi han al objmap en el secured resource
func updateSR (sr interface{}, objmap *map[string]interface{}  ) {
    var v reflect.Value
    v = reflect.ValueOf(sr)
    if v.Kind() != reflect.Ptr {
        log.Log("Must pass a pointer, not a value, to StructScan destination.")
    }

    s := reflect.Indirect(v)
    typeOfT, err := BaseStructType(s.Type())
    if err != nil {
        log.Loge(err)
    }

    var val interface{}
    var name string
    var ok bool
    for i := 0; i < s.NumField(); i++ {
        f := s.Field(i)
        name = typeOfT.Field(i).Name
        val, ok = (*objmap)[name]
        if ok {
            valr := reflect.ValueOf(val)
            f.Set(valr)
        }
    }

    /*

    fm, err := getFieldmap(base)
    if err != nil {
        log.Loge(err)
    }

    */
}

// Return the type of a struct, dereferencing it if it is a pointer. Returns
// an error if the destination is not a struct or a pointer to a struct.
func BaseStructType(t reflect.Type) (reflect.Type, error) {
    switch t.Kind() {
    case reflect.Ptr:
        t = t.Elem()
        fallthrough
    case reflect.Struct:
    default:
        return nil, errors.New("Destination must be a struct type.")
    }
    return t, nil
}

