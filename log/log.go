package log

import(
    "animalsbackend/conf"
    "os"
    "log"
    "time"
    "fmt"
)



func Log(m string){

    stamp := time.Now().UTC().Format(time.Stamp)

    f, err := os.OpenFile(conf.LogFile, os.O_APPEND|os.O_WRONLY, 0660)
    if err != nil {
        log.Print(err)
    }

    defer f.Close()
    s:=stamp + " : " + m + "\n"
    fmt.Print(s)

    if _, err = f.WriteString(s);
    err != nil {
        panic(err)
    }
}


func Loge(e error){
    Log(e.Error())
}


func CreateLogFile(){
    _, errOpen := os.OpenFile(conf.LogFile, os.O_APPEND|os.O_WRONLY, 0660)

    if(errOpen != nil){
        _, err := os.Create(conf.LogFile)
        if err != nil {
            log.Print(err)
        }
    }
}
