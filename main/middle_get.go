package main

import(
    "net/http"
    "encoding/json"
    "fmt"

    "animalsbackend/db"
    "github.com/gorilla/mux"
)

type Securer_get struct {
    handler     Handler_get
}

func (s *Securer_get) ServeHTTP(w http.ResponseWriter, r *http.Request) {


    session := db.GetSession(r)

    userid, ta_ok := session.Values["userid"].(int)
    if(!ta_ok){
        http.Error(w, "Bad cookie", 401)
        return
    }

    var res []db.SecuredResource = db.ToSR(s.handler(w, r))

    user := db.GetUser( userid )

    ok := true
    for _, r := range res {
        ok = ok && r.CanGet(user)
    }

    if(ok){
        response , _ := json.Marshal( res )
        fmt.Fprint(w,string(response))

    }else{
        http.Error(w, "No tienes suficientes permisos o no estas logeado", 401)
    }

}

type Handler_get func(http.ResponseWriter, *http.Request) interface{}

func AddGetRes(r *mux.Router, route string, handler Handler_get){
    r.Handle(route, &Securer_get{handler}).Methods("GET")
}






