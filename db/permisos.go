package db

type SecuredResource interface{
    CanGet(*User) bool
    CanPut(*User) bool
    CanDel(*User) bool
    CanUpd(*User, map[string]interface{}) bool
}

///////////////////

func (m Mascota) CanGet(u *User) bool {
    if(u.Id ==  m.Id_usuario){
        return true
    }else if (u.Permisos >= 200){
        user_pet := GetUser(m.Id_usuario)
        if user_pet.Id_vet == u.Id_vet {
            return true
        }
    }
    return false
}

func (us User) CanGet(u *User) bool {
    return true
}

func (v Veterinario) CanGet(u *User) bool {
    return true
}

func (d Diagnostico) CanGet(u *User) bool {
    return true
}

func (f Ficha) CanGet(u *User) bool {
    return true
}

func (f Foto) CanGet(u *User) bool {
    return true
}

///////////////////

func (m Mascota) CanPut(u *User) bool {
    return true
}

func (us User) CanPut(u *User) bool {
    return true
}

func (v Veterinario) CanPut(u *User) bool {
    return true
}

func (d Diagnostico) CanPut(u *User) bool {
    return true
}

func (f Ficha) CanPut(u *User) bool {
    return true
}

func (f Foto) CanPut(u *User) bool {
    return true
}

///////////////////

func (m Mascota) CanDel(u *User) bool {
    return m.CanPut(u)
}

func (us User) CanDel(u *User) bool {
    return us.CanPut(u)
}

func (v Veterinario) CanDel(u *User) bool {
    return v.CanPut(u)
}

func (d Diagnostico) CanDel(u *User) bool {
    return d.CanPut(u)
}

func (f Ficha) CanDel(u *User) bool {
    return f.CanPut(u)
}

func (f Foto) CanDel(u *User) bool {
    return f.CanPut(u)
}

///////////////////

func (m Mascota) CanUpd(u *User , up map[string]interface{}) bool {
    return true
}

func (us User) CanUpd(u *User, up map[string]interface{}) bool {
    return true
}

func (v Veterinario) CanUpd(u *User, up map[string]interface{}) bool {
    return true
}

func (d Diagnostico) CanUpd(u *User, up map[string]interface{}) bool {
    return true
}

func (f Ficha) CanUpd(u *User, up map[string]interface{}) bool {
    return true
}

func (f Foto) CanUpd(u *User, up map[string]interface{}) bool {
    return true
}



//////////////////


func CanPut( r interface{}, user *User) bool {

    res := ToSR(r)

    for _, r := range res {
        if(!r.CanPut(user)){
            return false
        }
    }
    return true
}


