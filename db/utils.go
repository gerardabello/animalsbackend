package db

import (
    "reflect"
    "animalsbackend/log"
    "fmt"

    "errors"
)

func ToSR(t interface{}) []SecuredResource{
    s := reflect.Indirect(reflect.ValueOf(t))

    if(s.Kind() != reflect.Slice){
        log.Log("Petara perque no es un slice")
    }

    var interfaceSlice []SecuredResource = make([]SecuredResource, s.Len())

    for i := 0; i < s.Len(); i++ {
        var ok bool
        interfaceSlice[i], ok = s.Index(i).Interface().(SecuredResource)

        if(!ok){
            log.Log("No era un slice del tipus SecuredResource")
        }
    }

    return interfaceSlice
}


func toFicha(t *SecuredResource) *Ficha{
    //TODO massa memoria?
    f :=  ((*t).(Ficha))
    return &f
}

func UpdateQuery(in *map[string]interface{}, t reflect.Type) string{
    var query string = "SET "
    var id string
    var inter interface{}
    var ok bool
    var tag string
    var name string

    for i := 0; i < t.NumField(); i++ {

        tag = t.Field(i).Tag.Get("db")
        name = t.Field(i).Name

        if tag == "id" {
            inter, ok := (*in)[name]
            if (!ok){
                log.Log("Updating dades que no tenen id!!!!!")
            }
            id = fmt.Sprint(inter)
        }else{

            inter, ok = (*in)[name]

            if(ok){
                query = query + tag + "=" + fmt.Sprint(inter) + ","

            }
        }
    }
    query = query[:len(query)-1]

    query = query + " WHERE Id = " + id

    return query
}

func getBaseQuery(t reflect.Type) (string, error){

    switch t {
    case TFicha:
        return baseQueryGetFicha, nil
    case TFoto:
        return baseQueryGetFoto, nil
    case TDiag:
        return baseQueryGetDiag, nil
    case TMascota:
        return baseQueryGetMascota, nil
    case TUser:
        return baseQueryGetUser, nil
    case TVet:
        return baseQueryGetVet, nil

    default:
        return "",errors.New("GetBaseQuery: No s'ha trobat cap tipus compatible : " + t.String())
    }

}


func postBaseQuery(t reflect.Type) (string, error){

    switch t {
    case TFicha:
        return baseQueryPostFicha, nil
    case TFoto:
        return baseQueryPostFoto, nil
        /*
    case TDiag:
        return baseQueryPostDiag, nil
    case TMascota:
        return baseQueryPostMascota, nil
    case TUser:
        return baseQueryPostUser, nil
    case TVet:
        return baseQueryPostVet, nil
        */

    default:
        return "",errors.New("PostBaseQuery: No s'ha trobat cap tipus compatible : " + t.String())
    }

}
