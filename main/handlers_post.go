package main

import(
    "net/http"
    "bytes"
    "strings"
    "animalsbackend/db"
    "animalsbackend/log"
    "animalsbackend/conf"
    "encoding/json"
    "encoding/base64"
    "io/ioutil"
    "strconv"
    "hash/crc64"
)


///UTILS


func saveB64(base string, path string) string{

    data, err2 := base64.StdEncoding.DecodeString(base)

    if err2 != nil {
        log.Loge(err2)
    }


    hash := crc64.Checksum(data, crc64.MakeTable(crc64.ECMA))

    fpath := path + "/" + strconv.FormatUint(hash,16) + ".jpg"

    err3 := ioutil.WriteFile(fpath, data, 0644)

    if err3 != nil {
        log.Loge(err3)
    }

    return fpath
}


////HANDLERS


func hPostFoto(w http.ResponseWriter, r *http.Request){
    user := db.GetSessionUser(r)

    body, err := ioutil.ReadAll(r.Body);

    if err != nil {
        log.Loge(err)
    }

    fi := bytes.IndexRune(body, '{')
    li := bytes.IndexRune(body, '}')

    object := body[fi:li+1]


    /*
    type FotoUpload struct {
        mascotaid int
        imageB64 string
    }

    foto := FotoUpload{}

    err2 := json.Unmarshal(object, &foto)

    if err2 != nil {
        log.Loge(err2)
    }

    */

    var objmap map[string]json.RawMessage
    err2 := json.Unmarshal(object, &objmap)

    if err2 != nil {
        log.Loge(err2)
    }


    b64 := strings.SplitAfter(string(objmap["imageB64"]),"base64,")[1]

    log.Log(b64)

    ft := db.Foto{}

    ft.Path = saveB64(b64,conf.BaseWeb + conf.PathFotos)

    ft.Path = strings.TrimPrefix(ft.Path, conf.BaseWeb + "panel/")

    var err3 error

    ft.Id_mascota, err3 = strconv.Atoi(string(objmap["idmascota"]))

    if err3 != nil {
        log.Loge(err3)
    }

    f := []*db.Foto{&ft}

    if(db.CanPut(f, user)) {
        db.PostFotos(f)
    }

}
