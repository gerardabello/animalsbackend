package db

import(
    "animalsbackend/log"
)

const (
    baseQueryGetVet = "SELECT id,subdominio,inicio,limite,nombre,direccion,cpostal,ciudad,provincia,pais,telf,logo FROM veterinarias "
)


func GetVeterinarioSub(subdominio string) ([]*Veterinario){
    vet := []*Veterinario{}

    getModels(&vet, baseQueryGetVet + "WHERE subdominio = ?", subdominio)

    if(len(vet)>1){
        log.Log("Mes de un veterinari té el subdomini: " + subdominio)
    }

    return vet
}


func GetVeterinario(id int) ([]*Veterinario){
    vet := []*Veterinario{}

    getModels(&vet, baseQueryGetVet + "WHERE id = ?", id)

    return vet
}

