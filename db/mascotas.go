package db

import(
    "strconv"
)


const(
    baseQueryGetMascota = "SELECT id, nombre, idusuario, nacimiento, sexo, idraza, peso, descripcion, nchip, foto FROM mascotas "
    baseQueryUpdMascota ="UPDATE mascotas "
)


func GetMascotasDeUsuario(id int) ([]*Mascota){
    mascotas := []*Mascota{}

    getModels(&mascotas, baseQueryGetMascota + "WHERE idusuario=?", id)

    return mascotas
}


func GetMascota(id int) ([]*Mascota){
    mascota := []*Mascota{}

    getModels(&mascota, baseQueryGetMascota + "WHERE id=?", id)

    return mascota
}


func UpdMascotaFoto(id int, path string) {
    exec(baseQueryUpdMascota + "SET foto = '" + path + "' WHERE id=" + strconv.Itoa(id))
}
