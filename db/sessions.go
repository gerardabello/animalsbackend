package db

import (
    "animalsbackend/conf"
    "github.com/gorilla/sessions"
    "net/http"
)

var store = sessions.NewCookieStore(conf.CookieAuthKey, conf.CookieEncKey)

func GetSession(r *http.Request) *sessions.Session {
    session, _ := store.Get(r, conf.CookieSessionName)
    /*
    if session.IsNew {
        session.Options.Domain = "api.webanimales.com"
        session.Options.HttpOnly = false
        session.Options.Secure = true
        //session.Options.MaxAge = 86400 * 7;  //7 dies
    }
    */

    return session
}


func GetSessionUser(r *http.Request) *User {

    session := GetSession(r)

    userid, _ := session.Values["userid"].(int)

    return GetUser( userid )
}
